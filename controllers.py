from peewee import DoesNotExist, DatabaseError

from helpers import generate_board, check_is_hit, update_ships_on_board

# constants
OK_CODE = 200
OK_CREATED_CODE = 201
BAD_DATA_CODE = 400
NOT_FOUND_CODE = 404
SERVER_ERROR_CODE = 500

SHIPS_LIMIT = 8


def get_board_info(board_id, board_model):
    """
    Fetches game board information.

    :param board_id: id to of the board to fetch
    :param board_model: model to use
    :return:response with board info, code
    """
    try:
        board = board_model.get(board_model.id == board_id)
        resp = {'board_id': board_id, 'number_of_ships': board.number_of_ships}
        code = OK_CODE
    except DoesNotExist:
        resp = {'error': 'Board with id {} not found'.format(board_id)}
        code = NOT_FOUND_CODE
    return resp, code


def create_new_board(payload, board_model):
    """
    Creates new game board for game.

    :param payload: dict with `number_of_ships` parameter.
    :param board_model: model_to_use
    :return: response with number of ships and id of the created board.
    """
    number_of_ships = payload.get('number_of_ships')
    try:
        if number_of_ships is None or number_of_ships == 0:
            resp = {'error': 'Number of ships cannot be zero'}
            code = BAD_DATA_CODE
        elif number_of_ships > SHIPS_LIMIT:
            resp = {'error': 'Number of ships cannot be greater than {}'.format(SHIPS_LIMIT)}
            code = BAD_DATA_CODE
        else:
            battlefield = generate_board(number_of_ships)
            try:
                board = board_model.create(number_of_ships=number_of_ships,
                                           battle_field={'battlefield': battlefield})
                resp = {'board_id': board.id, 'number_of_ships': board.number_of_ships}
                code = OK_CREATED_CODE
            except DatabaseError:
                resp = {'error': 'Internal error'}
                code = SERVER_ERROR_CODE
    except TypeError:
        resp = {'error': 'Number of ships must be integer'}
        code = BAD_DATA_CODE
    return resp, code


def update_board(board_id, battle_field, board_model):
    """
    Performs update inside board with new battle field.

    :param board_id: id of the game board.
    :param board_model: model to use
    :param battle_field: new battle field for update
    """
    battle_field = {'battlefield': battle_field}
    try:
        q = board_model.update({board_model.battle_field: battle_field,
                                board_model.number_of_ships: board_model.number_of_ships - 1}).where(
            board_model.id == board_id)
        q.execute()
        return True
    except DatabaseError:
        return False


def make_turn(board_id, payload, board_model):
    """
    Combines logic, required to make turn.
    Checks if there is hit and returns proper response. If there is no ships left, marks board as
    already played.

    :param board_id: game board to play
    :param payload: ship position information
    :param board_model: model to use
    :return: response with info about turn.
    """
    pos = payload.get('position')
    try:
        board = board_model.get(board_model.id == board_id)
        bf = board.battle_field
        if not bf['battlefield']:
            resp = {'info': 'Win! Board with id {} is already finished'.format(board_id)}
            code = OK_CODE
        else:
            is_hit = check_is_hit(pos, bf['battlefield'])
            if is_hit:
                bf = update_ships_on_board(pos, bf['battlefield'])
                board = update_board(board_id, bf, board_model)
            resp = {'board_id': board_id, 'hit': is_hit, 'ships_left': board.number_of_ships,
                    'hit_position': pos}
            code = OK_CODE
    except DoesNotExist:
        resp = {'error': 'Board with id {} not found'.format(board_id)}
        code = NOT_FOUND_CODE
    return resp, code
