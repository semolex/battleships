from flask import Flask, jsonify, make_response, request
from peewee import OperationalError

from models import database, GameBoard
from controllers import get_board_info, create_new_board, make_turn, OK_CODE

app = Flask(__name__)


def respond(code, resp):
    """
    Unifies response for all API requests.
    :param code: HTTP code for response
    :param resp: response body
    :return: Flask response
    """
    return make_response(jsonify(resp), code)


@app.before_request
def before_request():
    """
    Pre-script that will be used before each request.
    Database will be opened before each request.
    """
    try:
        database.connect()
    except OperationalError:
        print('Connection already opened. Proceeding.')


@app.after_request
def after_request(response):
    """
    Pre-script that will be used after each request.
    Database will be closed after each request.
    """
    database.close()
    return response


@app.route('/', methods=['GET'])
def welcome():
    """
    Default route that will show greeting.
    """
    return respond(OK_CODE, {'Welcome': 'It is very simple REST-like battleships API'})


@app.route('/board/', methods=['POST'])
def create_board():
    """
    View that creates board via passed arguments and POST method.
    """
    payload = request.get_json(force=True)
    resp, code = create_new_board(payload, GameBoard)
    return respond(code, resp)


@app.route('/board/<int:board_id>', methods=['GET', 'PUT'])
def get_or_update(board_id):
    """
    Fetches or updates game board via GET\PUT method.

    :param board_id: id of the game board to process
    """
    resp, code = None, None
    if request.method == 'GET':
        resp, code = get_board_info(board_id, GameBoard)
    elif request.method == 'PUT':
        payload = request.get_json(force=True)
        resp, code = make_turn(board_id, payload, GameBoard)
    return respond(code, resp)


if __name__ == '__main__':
    app.run()
