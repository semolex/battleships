import peewee
from playhouse.sqlite_ext import JSONField

database = peewee.SqliteDatabase('battleships.db')


class BaseModel(peewee.Model):
    """
    Base model for Game Boards
    """

    class Meta:
        database = database


class GameBoard(BaseModel):
    """
    Game board model. Holds required info for game.
    """
    number_of_ships = peewee.IntegerField()
    battle_field = JSONField()


def create_tables():
    """
    Creates table for game.
    """
    with database:
        database.create_tables([GameBoard])
