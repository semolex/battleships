import random
import string

BOARD_LETTERS_RANGE = 12  # number of letter to use for cell generating.


def generate_board(n):
    """
    Generates new board with ships.
    :param n: number of ships.
    :return: list with ships position.
    """
    board = []
    for i in range(n):
        board.append(
            '{}:{}'.format(random.choice(string.ascii_uppercase),
                           random.choice(range(1, BOARD_LETTERS_RANGE))))
    board = list(set(board))
    return board


def check_is_hit(pos, board):
    """
    Checks if there is hit to the enemy ship.
    :param pos: position to check.
    :param board: game board list with ships position.
    :return: bool that indicates if there is a hit.
    """
    if pos in board:
        return True
    else:
        return False


def update_ships_on_board(pos, board):
    """
    Deletes damaged ship from board.
    :param pos: position of the ship to remove
    :param board: board with ships position.
    :return: updated board
    """
    del board[board.index(pos)]
    return board
