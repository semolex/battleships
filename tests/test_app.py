import unittest
from unittest import mock

from models import create_tables, GameBoard
from controllers import OK_CODE, OK_CREATED_CODE, BAD_DATA_CODE, NOT_FOUND_CODE

import peewee

from run import app


WELCOME_RESP = b'{"Welcome":"It is very simple REST-like battleships API"}\n'
CREATED_RESPONSE = b'{"board_id":1,"number_of_ships":5}\n'
BAD_DATA_RESPONSE_1 = b'{"error":"Number of ships cannot be zero"}\n'
BAD_DATA_RESPONSE_2 = b'{"error":"Number of ships cannot be greater than 8"}\n'
BOARD_NOT_FOUND_RESPONSE = b'{"error":"Board with id 1 not found"}\n'
BOARD_UPDATE_RESPONSE = b'{"board_id":1,"hit":false,"hit_position":"Y:2","ships_left":5}\n'


class BattleShipsTestCase(unittest.TestCase):

    def setUp(self):

        app.testing = True
        self.db = peewee.SqliteDatabase(':memory:')
        with mock.patch('models.database', self.db):
            create_tables()
        self.app = app.test_client()

    def tearDown(self):
        self.db.drop_tables([GameBoard])

    def test_welcome_url(self):
        resp = self.app.get('/')
        assert WELCOME_RESP in resp.data

    def test_create_url_ok(self):
        resp = self.app.post('/board/', data='{"number_of_ships": 5}')
        assert resp.status_code == OK_CREATED_CODE
        assert resp.data == CREATED_RESPONSE

    def test_create_url_bad_data(self):
        resp = self.app.post('/board/', data='{"unknown_param": 5}')
        assert resp.status_code == BAD_DATA_CODE
        assert resp.data == BAD_DATA_RESPONSE_1

    def test_create_url_bad_data_2(self):
        resp = self.app.post('/board/', data='{"number_of_ships": 9}')
        assert resp.status_code == BAD_DATA_CODE
        assert resp.data == BAD_DATA_RESPONSE_2

    def test_get_or_update_get(self):
        self.app.post('/board/', data='{"number_of_ships": 5}')
        resp = self.app.get('/board/1')
        assert resp.status_code == OK_CODE
        assert resp.data == CREATED_RESPONSE

    def test_get_or_update_get_not_found(self):
        resp = self.app.get('/board/1')
        assert resp.status_code == NOT_FOUND_CODE
        assert resp.data == BOARD_NOT_FOUND_RESPONSE

    def test_get_or_update_update(self):
        self.app.post('/board/', data='{"number_of_ships": 5}')
        resp = self.app.put('/board/1', data='{"position": "Y:2"}')
        assert resp.status_code == OK_CODE
        assert resp.data == BOARD_UPDATE_RESPONSE

    def test_get_or_update_update_not_found(self):
        resp = self.app.put('/board/1', data='{"position": "Y:2"}')
        assert resp.status_code == NOT_FOUND_CODE
        assert resp.data == BOARD_NOT_FOUND_RESPONSE


if __name__ == '__main__':
    unittest.main()
